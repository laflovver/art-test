﻿Shader "HyperShaders/Highlight"
{
    SubShader
    {
        Tags { "RenderType"="Transparent" }
        LOD 100

        Pass
        {
            ZWrite Off
            ZTest On
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off

            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            float4 vert (float4 vertex : POSITION) : SV_POSITION
            {
                return UnityObjectToClipPos(vertex);
            }

            fixed4 frag (float4 vertex : SV_POSITION) : SV_Target
            {
                return float4(1.0f, 1.0f, 1.0f, 0.5f);
            }
            
            ENDCG
        }
    }
}
