// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "HyperShaders/UI/Default"
{
    Properties
    {
        _Perspective ("Perspective", Range(0, 1)) = 0
        _PerspectiveNearPlane ("Perspective Near Plane", Float) = 0.0
        _AnchorOffset ("Anchor Offset", Vector) = (0,0,0,0)
        _CustomLightDir("Custom Light Dir", Vector) = (0,0,1,0)
        _CustomLightColor("Custom Light Color", Color) = (1,1,1,1)

        [Space]
        _MainColor ("Main Color", Color) = (1.0,1.0,1.0,1.0)
        _ShadowColor ("Shadow Color", Color) = (0.5,0.5,0.5,1.0)

        [Space]
        [Toggle(SF_MAIN_TEXTURE)] _SF_MAIN_TEXTURE("Main texture", Int) = 0
        _MainTex ("Main Texture", 2D) = "white" {}

        [Space]
        [Toggle(SF_DIFFUSE)] _SF_DIFFUSE("Diffuse", Int) = 0
        [Toggle(SF_DIFFUSE_RAMP)] _SF_DIFFUSE_RAMP("Diffuse ramp", Int) = 0
        _MainColorRampTex ("Diffuse Ramp Map", 2D) = "white" {}

        [Space]
        [Toggle(SF_RIM_LIGHT)] _SF_RIM_LIGHT("Rim Light", Int) = 0
        _RimColor ("Rim Color", Color) = (1.0,1.0,1.0,1.0)
        _RimLightPower ("Rim Light Power", Float) = 1.0

        [Space]
        [Toggle(SF_SPECULAR)] _SF_SPECULAR("Specular", Int) = 0
        _SpecTex ("Specular Texture", 2D) =  "white" {} 
        _SpecShininess ("Specular Shininess", Range(0, 1)) = 0.5
        _SpecIntensity ("Specular Intensity", Color) = (1,1,1,1)

        [Space]
        [Toggle(SF_REFLECTION)] _SF_REFLECTION("Reflection", Int) = 0
        _ReflectionTex ("Reflection Texture", 2D) =  "white" {} 
        _ReflectionCube ("Reflection Cube", Cube) = "" {}

        [Space]
        [Toggle(SF_EMISSION)] _SF_EMISSION("Emission", Int) = 0
        _EmissionMap ("Emission Map", 2D) = "black" {}
        _EmissionIntensity ("Emission Intensity", Range(0, 1)) = 1

        [Space]
        [Toggle(SF_COLOR_LERP)] _SF_COLOR_LERP("Color Lerp", Int) = 0
        _ColorToLerp ("Color To Lerp", Color) = (1,1,1,1)
        _LerpValue ("Lerp Value", Range(0.0, 1.0)) = 0.0
        
        [Space]
        [Toggle(SF_COLOR_FILL)] _SF_COLOR_FILL("Color Z Fill", Int) = 0
        [Toggle(SF_SCREEN_SPACE_FILL)] _SF_SCREEN_SPACE_FILL("Screen Space Fill", Int) = 0
        _FillLerpStart ("Fill Lerp Start", Float) = 0.0
        _FillLerpEnd ("Fill Lerp End", Float) = 0.0
        _PlaneRotationX ("X Plane Rotation", Float) = 0.0
        [Toggle(InverseFill)] _InverseFill ("InverseFill", Int) = 0 
        _InactiveFillColor ("Inactive Color", Color) = (0.7,0.7,0.7, 1.0)
        _FillProgress ("Fill Progress", Range(0.0, 1.0)) = 0.0
        
        [Space]
        _Stencil ("Stencil ID", Float) = 0
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilReadMask ("Stencil Read Mask", Float) = 255
        [Enum(UnityEngine.Rendering.CompareFunction)] _StencilComp ("Stencil Comparison", Float) = 8
        [Enum(UnityEngine.Rendering.StencilOp)] _StencilOp ("Stencil Operation", Float) = 0
        
        _ColorMask ("Color Mask", Float) = 15
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
        }

        Stencil
        {
            Ref [_Stencil]
            Comp [_StencilComp]
            Pass [_StencilOp]
            ReadMask [_StencilReadMask]
            WriteMask [_StencilWriteMask]
        }
        
        ColorMask [_ColorMask]

        Lighting Off
        ZTest On
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            Name "Default"
        CGPROGRAM
            #define SF_UI_RENDERING 1

            #pragma vertex DefaultVertexShader
            #pragma fragment DefaultFragmentShader
            #pragma target 2.0

            #include "UnityCG.cginc"
            //#include "UnityUI.cginc"

            #pragma shader_feature SF_MAIN_TEXTURE
            #pragma shader_feature SF_DIFFUSE
            #pragma shader_feature SF_DIFFUSE_RAMP
            #pragma shader_feature SF_RIM_LIGHT
            #pragma shader_feature SF_SPECULAR
            #pragma shader_feature SF_REFLECTION
            #pragma shader_feature SF_EMISSION
            #pragma shader_feature SF_COLOR_LERP
            #pragma shader_feature SF_COLOR_FILL
            #pragma shader_feature SF_SCREEN_SPACE_FILL

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                float2 texcoord  : TEXCOORD0;
                float4 worldPosition : TEXCOORD1;
                UNITY_VERTEX_OUTPUT_STEREO
            };


            #include "CommonShaders.cginc"

        ENDCG
        }
    }
}
