Shader "HyperShaders/Default With Clip"
{ 
    Properties
    {
        _MainColor ("Main Color", Color) = (1.0,1.0,1.0,1.0)
        _ShadowColor ("Shadow Color", Color) = (0.5,0.5,0.5,1.0)

        _MainTex ("Main Texture", 2D) = "white" {}

        _MainColorRampTex ("Diffuse Ramp Map", 2D) = "white" {}

        _RimColor ("Rim Color", Color) = (1.0,1.0,1.0,1.0)
        _RimLightPower ("Rim Light Power", Float) = 1.0

        _SpecTex ("Specular Texture", 2D) =  "white" {} 
        _SpecShininess ("Specular Shininess", Range(0, 1)) = 0.5
        _SpecIntensity ("Specular Intensity", Color) = (1,1,1,1)

        _ReflectionTex ("Reflection Texture", 2D) =  "white" {} 
        _ReflectionCube ("Reflection Cube", Cube) = "" {}

        _EmissionMap("Emission Map", 2D) = "black" {}
        _EmissionIntensity("Emission Intensity", Range(0, 1)) = 1

        _ColorToLerp("Color To Lerp", Color) = (1,1,1,1)
        _LerpValue ("Lerp Value", Range(0.0, 1.0)) = 0.0

        _ColorR ("Color R", Color) = (1,1,1,1)
        _ColorG ("Color G", Color) = (1,1,1,1)
        _ColorB ("Color B", Color) = (1,1,1,1)
        
        _FillAmount ("Fill Amount", Range(0,1)) = 0.0
        _FillMaxAmount ("Fill Max Amount", Range(0,5)) = 0.0
        
        _BottomPos ("Bottom Position", Vector) = (0.0,0.0,0.0,0.0)
    }
    
    SubShader
    {
        Tags { "RenderType" = "Opaque"}

        Pass
        {
            Tags { "LightMode" = "ForwardBase" }

            CGPROGRAM
                      
            #pragma multi_compile_fwdbase
            #pragma vertex DefaultVertexShader
            #pragma fragment DefaultFragmentShader
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_fog
            #pragma multi_compile_instancing
            #pragma instancing_options nolodfade nolightprobe nolightmap

            #pragma shader_feature SF_MAIN_TEXTURE
            #pragma shader_feature SF_DIFFUSE
            #pragma shader_feature SF_DIFFUSE_RAMP
            #pragma shader_feature SF_RIM_LIGHT
            #pragma shader_feature SF_SPECULAR
            #pragma shader_feature SF_REFLECTION
            #pragma shader_feature SF_EMISSION
            #pragma shader_feature SF_COLOR_LERP
            #pragma shader_feature SF_PAINT_LAYER

            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "UnityLightingCommon.cginc"

            UNITY_INSTANCING_BUFFER_START(Props)
            UNITY_DEFINE_INSTANCED_PROP(fixed4, _MainColor)
            UNITY_DEFINE_INSTANCED_PROP(fixed4, _ShadowColor)
            
            #if SF_RIM_LIGHT
                UNITY_DEFINE_INSTANCED_PROP(fixed4, _RimColor)
                UNITY_DEFINE_INSTANCED_PROP(fixed, _RimLightPower)
            #endif
            #if SF_SPECULAR
                UNITY_DEFINE_INSTANCED_PROP(float, _SpecShininess)
                UNITY_DEFINE_INSTANCED_PROP(float4, _SpecIntensity)
            #endif
            #if SF_REFLECTION
                UNITY_DEFINE_INSTANCED_PROP(fixed4, _ReflectionColor)           
            #endif
            #if SF_EMISSION
                UNITY_DEFINE_INSTANCED_PROP(float, _EmissionIntensity)
            #endif
            #if SF_COLOR_LERP
                UNITY_DEFINE_INSTANCED_PROP(fixed3, _ColorToLerp)
                UNITY_DEFINE_INSTANCED_PROP(float, _LerpValue)
            #endif
            #if SF_PAINT_LAYER
                UNITY_DEFINE_INSTANCED_PROP(fixed4, _ColorR)
                UNITY_DEFINE_INSTANCED_PROP(fixed4, _ColorG)
                UNITY_DEFINE_INSTANCED_PROP(fixed4, _ColorB)
            #endif
            UNITY_INSTANCING_BUFFER_END(Props)

            #if SF_MAIN_TEXTURE
                sampler2D _MainTex;
                float4 _MainTex_ST;
            #endif

            #if SF_DIFFUSE && SF_DIFFUSE_RAMP
                sampler1D _MainColorRampTex;
            #endif

            
            #if SF_SPECULAR
                sampler2D _SpecTex;
            #endif
            
            #if SF_REFLECTION
                sampler2D _ReflectionTex;
                samplerCUBE _ReflectionCube;        
            #endif
            
            #if SF_EMISSION
                sampler2D _EmissionMap;
            #endif
            

            #if SF_PAINT_LAYER
                uniform sampler2D _PaintTex;
                uniform float3 _PaintCameraPosition;
                uniform float _PaintCameraSize;
                uniform float _PaintFade;
            #endif
                       
            float _FillAmount, _FillMaxAmount;
            float4 _BottomPos;

            struct Input
            {
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
                float3 lightDir : TEXCOORD1;
                float3 worldNormal : TEXCOORD2;
                float3 viewDir : TEXCOORD3;
                float3 worldPos : TEXCOORD4;
                float fillEdge : TEXCOORD5;

                #ifndef UNITY_UI_CLIP_RECT
                    LIGHTING_COORDS(5,6)
                    UNITY_FOG_COORDS(7)
                #endif
            };
    
   
            float4 ObjectToWorldPos(float3 pos)
            {
                return mul(unity_ObjectToWorld, float4(pos, 1.0));
            }

            float4 WorldToClipPos(float4 pos)
            {
                return mul(UNITY_MATRIX_VP, pos);
            }
    
            Input DefaultVertexShader(appdata_full v)
            {
                UNITY_SETUP_INSTANCE_ID(v);

                Input o;
                UNITY_INITIALIZE_OUTPUT(Input, o);
                UNITY_TRANSFER_INSTANCE_ID(v, o);

                float4 worldPos = ObjectToWorldPos(v.vertex);
                o.lightDir = UnityWorldSpaceLightDir(worldPos);
               
                #if SF_MAIN_TEXTURE
                    o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                #else
                    o.uv = v.texcoord;
                #endif
            
                o.pos = WorldToClipPos(worldPos);
                o.worldPos = worldPos.xyz;
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.viewDir = UnityWorldSpaceViewDir(worldPos);
                o.fillEdge = worldPos.y - _BottomPos.y + (1.0 - _FillAmount) * _FillMaxAmount;
                
                return o;
            }
    
            half4 DefaultFragmentShader(Input IN) : COLOR
            {
                if (step(IN.fillEdge, 0.5) > 0)
                {
                    discard;
                }
                
                IN.lightDir = normalize(IN.lightDir);
                IN.worldNormal = normalize(IN.worldNormal);
                IN.viewDir = normalize(IN.viewDir);
            
                float NdotLUnclamped = dot(IN.worldNormal, IN.lightDir);
                UNITY_LIGHT_ATTENUATION(atten, IN, 0);
                float3 lightColor =  _LightColor0.rgb;
                float NdotL = saturate(NdotLUnclamped);
            
                float4 albedo = UNITY_ACCESS_INSTANCED_PROP(Props, _MainColor);
                float3 color = 0.0;
            
                #if SF_MAIN_TEXTURE
                    albedo *= tex2D(_MainTex, IN.uv);
                #endif
            
                #if SF_DIFFUSE
                    float3 diffuse = albedo.rgb * lightColor;
                    #if SF_DIFFUSE_RAMP
                        float ramp = max(0.5 - NdotLUnclamped * 0.5, 0.5 - atten * 0.5);
                        diffuse.rgb *=  tex1D(_MainColorRampTex, ramp).rgb;
                    #else
                        diffuse = lerp(UNITY_ACCESS_INSTANCED_PROP(Props, _ShadowColor).rgb * diffuse, diffuse, NdotL * atten);
                    #endif
                    color.rgb += diffuse;
                #else
                    color.rgb += albedo;
                #endif
            
                #if SF_RIM_LIGHT
                    float3 rim = albedo.rgb * UNITY_ACCESS_INSTANCED_PROP(Props, _RimColor).rgb * pow(1.0 - dot(IN.worldNormal, IN.viewDir), UNITY_ACCESS_INSTANCED_PROP(Props, _RimLightPower));
                    color.rgb += rim;
                #endif
            
                float4 resultColor = float4(color, albedo.a);
            
                #if SF_REFLECTION
                    float3 reflectionColor = tex2D(_ReflectionTex, IN.uv);
                    float3 worldReflection = reflect(-IN.viewDir, IN.worldNormal);
                    resultColor.rgb += texCUBE(_ReflectionCube, worldReflection).rgb * reflectionColor;
                #endif
                
                #if SF_SPECULAR
                    float3 specularReflection;
                    if (NdotL < 0.0) 
                    {
                        specularReflection = 0.0;
                    } 
                    else 
                    {
                        specularReflection = tex2D(_SpecTex, IN.uv) * atten * UNITY_ACCESS_INSTANCED_PROP(Props, _SpecIntensity) *
                            pow(max(0.0, dot(reflect(-IN.lightDir, IN.worldNormal), IN.viewDir)),
                                (UNITY_ACCESS_INSTANCED_PROP(Props, _SpecShininess) * 199.0 + 1.0));
                    }
                    resultColor.rgb += specularReflection; 
                #endif
            
                #if SF_EMISSION
                    resultColor.rgb += tex2D(_EmissionMap, IN.uv).rgb * UNITY_ACCESS_INSTANCED_PROP(Props, _EmissionIntensity);
                #endif
            
                #if SF_COLOR_LERP
                    resultColor.rgb = lerp(resultColor.rgb, _ColorToLerp, UNITY_ACCESS_INSTANCED_PROP(Props, _LerpValue));
                #endif
           
                return resultColor;
            }
    
            uniform float4x4 _World2Receiver;
            uniform float4 _MatrixShadowColor;
            
            float4 ShadowMatrixVertexShader(float4 vertex : POSITION) : SV_POSITION
            {
               float4x4 modelMatrix = unity_ObjectToWorld;
            
                float4 lightDirection;
                if (_WorldSpaceLightPos0.w != 0.0) 
                {
                   lightDirection = normalize(mul(modelMatrix, vertex - _WorldSpaceLightPos0));
                } 
                else 
                {
                   lightDirection = -normalize(_WorldSpaceLightPos0); 
                }
            
               float4 vertexInWorldSpace = mul(modelMatrix, vertex);
               float4 world2ReceiverRow1 = _World2Receiver[1];
               float distanceOfVertex = dot(world2ReceiverRow1, vertexInWorldSpace); 
               float lengthOfLightDirectionInY = dot(world2ReceiverRow1, lightDirection); 
            
               if (distanceOfVertex > 0.0 && lengthOfLightDirectionInY < 0.0)
               {
                  lightDirection = lightDirection * (distanceOfVertex / (-lengthOfLightDirectionInY));
               }
               else
               {
                  lightDirection = float4(0.0, 0.0, 0.0, 0.0); 
               }
            
               return mul(UNITY_MATRIX_VP, vertexInWorldSpace + lightDirection);
            }
    
            float4 ShadowMatrixFragmentShader() : COLOR 
            {
               return _MatrixShadowColor;
            }                     
 
            ENDCG
        }
    }
 
    FallBack "Diffuse"
}