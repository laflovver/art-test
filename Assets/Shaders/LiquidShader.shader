Shader "HyperShaders/Liquid"
{ 
    Properties
    {
        _MainColor ("Main Color", Color) = (1.0,1.0,1.0,1.0)
        _ShadowColor ("Shadow Color", Color) = (0.5,0.5,0.5,1.0)

        [Toggle(SF_MAIN_TEXTURE)] _SF_MAIN_TEXTURE("Main texture", Int) = 0
        _MainTex ("Main Texture", 2D) = "white" {}

        [Toggle(SF_DIFFUSE)] _SF_DIFFUSE("Diffuse", Int) = 0
        [Toggle(SF_DIFFUSE_RAMP)] _SF_DIFFUSE_RAMP("Diffuse ramp", Int) = 0
        _MainColorRampTex ("Diffuse Ramp Map", 2D) = "white" {}

        [Toggle(SF_RIM_LIGHT)] _SF_RIM_LIGHT("Rim Light", Int) = 0
        _RimColor ("Rim Color", Color) = (1.0,1.0,1.0,1.0)
        _RimLightPower ("Rim Light Power", Float) = 1.0

        [Toggle(SF_SPECULAR)] _SF_SPECULAR("Specular", Int) = 0
        _SpecTex ("Specular Texture", 2D) =  "white" {} 
        _SpecShininess ("Specular Shininess", Range(0, 1)) = 0.5
        _SpecIntensity ("Specular Intensity", Color) = (1,1,1,1)

        [Toggle(SF_REFLECTION)] _SF_REFLECTION("Reflection", Int) = 0
        _ReflectionTex ("Reflection Texture", 2D) =  "white" {} 
        _ReflectionCube ("Reflection Cube", Cube) = "" {}

        [Toggle(SF_EMISSION)] _SF_EMISSION("Emission", Int) = 0
        _EmissionMap("Emission Map", 2D) = "black" {}
        _EmissionIntensity("Emission Intensity", Range(0, 1)) = 1

        [Toggle(SF_COLOR_LERP)] _SF_COLOR_LERP("Color Lerp", Int) = 0
        _ColorToLerp("Color To Lerp", Color) = (1,1,1,1)
        _LerpValue ("Lerp Value", Range(0.0, 1.0)) = 0.0
        
        _FillAmount ("Fill Amount", Range(0,1)) = 0.0
        _FillMaxAmount ("Fill Max Amount", Range(0,5)) = 0.0
             
        _FoamWidth ("Foam Line Width", Range(0,0.1)) = 0.0
        _FoamColor ("Foam Line Color", Color) = (1,1,1,1)
        
        _AngularSpeed ("Angular Speed", Range(0,1)) = 0.5
        _BottomPos ("Bottom Position", Vector) = (0.0,0.0,0.0,0.0)

        _StencilRef ("Stencil Ref Value", Float) = 0
        [Enum(UnityEngine.Rendering.CompareFunction)] _StencilComp ("Stencil Comparison", Float) = 6
        [Enum(UnityEngine.Rendering.StencilOp)] _StencilOp ("Stencil Operation", Float) = 0
        [Enum(UnityEngine.Rendering.StencilOp)] _StencilFail ("Stencil Fail", Float) = 0
        [Enum(UnityEngine.Rendering.StencilOp)] _StencilZFail ("Stencil ZFail", Float) = 0
    }
    
    SubShader
    {
        Pass
        {
        Tags
        { 
            "Queue"="Geometry+100" 
            "RenderType"="Geometry" 
            "IgnoreProjector"="True" 
        }

            Zwrite On
		    Cull Off

            Stencil
            {
                Ref [_StencilRef]
                Comp [_StencilComp]
                Pass [_StencilOp] 
                Fail [_StencilFail]
                ZFail [_StencilZFail]
            }

            CGPROGRAM

            #pragma multi_compile_fwdbase
            #pragma vertex vert
            #pragma fragment frag
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_fog
            #pragma multi_compile_instancing
            #pragma instancing_options nolodfade nolightprobe nolightmap
            
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "UnityLightingCommon.cginc"

            #pragma shader_feature SF_MAIN_TEXTURE
            #pragma shader_feature SF_DIFFUSE
            #pragma shader_feature SF_DIFFUSE_RAMP
            #pragma shader_feature SF_RIM_LIGHT
            #pragma shader_feature SF_SPECULAR
            #pragma shader_feature SF_REFLECTION
            #pragma shader_feature SF_EMISSION
            
            UNITY_DEFINE_INSTANCED_PROP(fixed4, _MainColor)
            
            UNITY_DEFINE_INSTANCED_PROP(fixed4, _RimColor)
            UNITY_DEFINE_INSTANCED_PROP(fixed, _RimLightPower)
            
            UNITY_DEFINE_INSTANCED_PROP(float, _SpecShininess)
            UNITY_DEFINE_INSTANCED_PROP(float4, _SpecIntensity)
            
            UNITY_DEFINE_INSTANCED_PROP(float, _EmissionIntensity)
            
            UNITY_DEFINE_INSTANCED_PROP(fixed4, _FoamColor)
            
            sampler2D _MainTex;
            float4 _MainTex_ST;

            sampler1D _MainColorRampTex;
            sampler2D _SpecTex;
    
            sampler2D _ReflectionTex;
            samplerCUBE _ReflectionCube;        
    
            sampler2D _EmissionMap;
            
            float _FillAmount, _FillMaxAmount, _FoamWidth, _AngularSpeed;
            float4 _BottomPos;
            
            struct Input
            {
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
                float3 lightDir : TEXCOORD1;
                float3 worldNormal : TEXCOORD2;
                float3 viewDir : TEXCOORD3;
                float3 worldPos : TEXCOORD4;  
                float fillEdge : TEXCOORD5;         
                
                #ifndef UNITY_UI_CLIP_RECT
                    LIGHTING_COORDS(5,6)
                    UNITY_FOG_COORDS(7)
                #endif
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };                                              
            
            float4 ObjectToWorldPos(float3 pos)
            {
                return mul(unity_ObjectToWorld, float4(pos, 1.0));
            }
            
            
            Input vert(appdata_full v)
            {
                UNITY_SETUP_INSTANCE_ID(v);
        
                Input o;
                UNITY_INITIALIZE_OUTPUT(Input, o);
                UNITY_TRANSFER_INSTANCE_ID(v, o);
        
                float4 worldPos = ObjectToWorldPos(v.vertex);       
                o.lightDir = UnityWorldSpaceLightDir(worldPos);                       
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);           
                o.pos = mul(UNITY_MATRIX_VP, worldPos);
                o.worldPos = worldPos.xyz;
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.viewDir = UnityWorldSpaceViewDir(worldPos);                             
                o.fillEdge = worldPos.y - _BottomPos.y + (1.0 - _FillAmount) * _FillMaxAmount;
                
                return o;
            }
            
            half4 frag(Input IN, fixed facing : VFACE) : COLOR
            {
                IN.lightDir = normalize(IN.lightDir);
                IN.worldNormal = normalize(IN.worldNormal);
                IN.viewDir = normalize(IN.viewDir);
            
                float NdotLUnclamped = dot(IN.worldNormal, IN.lightDir);
                UNITY_LIGHT_ATTENUATION(atten, IN, IN.worldPos.xyz)
                // float atten = LIGHT_ATTENUATION(IN);
                float3 lightColor =  _LightColor0.rgb;
                float NdotL = saturate(NdotLUnclamped);
            
                float4 albedo = UNITY_ACCESS_INSTANCED_PROP(Props, _MainColor);
                float3 color = 0.0;
            
                albedo *= tex2D(_MainTex, IN.uv);
            
                float3 diffuse = albedo.rgb * lightColor;
                float ramp = max(0.5 - NdotLUnclamped * 0.5, 0.5 - atten * 0.5);
                diffuse.rgb *=  tex1D(_MainColorRampTex, ramp).rgb;
                color.rgb += diffuse;
            
                float3 rim = albedo.rgb * UNITY_ACCESS_INSTANCED_PROP(Props, _RimColor).rgb * pow(1.0 - dot(IN.worldNormal, IN.viewDir), UNITY_ACCESS_INSTANCED_PROP(Props, _RimLightPower));
                color.rgb += rim;
            
                float4 resultColor = float4(color, albedo.a);
            
                float3 reflectionColor = tex2D(_ReflectionTex, IN.uv);
                float3 worldReflection = reflect(-IN.viewDir, IN.worldNormal);
                resultColor.rgb += texCUBE(_ReflectionCube, worldReflection).rgb * reflectionColor;
                
                float3 specularReflection;
                if (NdotL < 0.0) 
                {
                    specularReflection = 0.0;
                } 
                else 
                {
                    specularReflection = tex2D(_SpecTex, IN.uv) * atten * UNITY_ACCESS_INSTANCED_PROP(Props, _SpecIntensity) *
                        pow(max(0.0, dot(reflect(-IN.lightDir, IN.worldNormal), IN.viewDir)),
                        (UNITY_ACCESS_INSTANCED_PROP(Props, _SpecShininess) * 199.0 + 1.0));
                }
                 resultColor.rgb += specularReflection; 
            
                resultColor.rgb += tex2D(_EmissionMap, IN.uv).rgb * UNITY_ACCESS_INSTANCED_PROP(Props, _EmissionIntensity);

                if (step(IN.fillEdge, 0.5) < 1)
                {
                    discard;
                }
                
                float4 foam = ( step(IN.fillEdge, 0.5) - step(IN.fillEdge, (0.5 - _FoamWidth)));
                float4 foamColored = foam * saturate(_MainColor * 1.25);
                
                float4 result = step(IN.fillEdge, 0.5) - foam;      
                resultColor = result * resultColor + foamColored;
                
                float4 topColor = saturate(_MainColor * 1.25) * (foam + result);
        
                return facing > 0 ? resultColor : topColor;
            }
 
            ENDCG
        }
    }
 
 
    FallBack "Diffuse"
}